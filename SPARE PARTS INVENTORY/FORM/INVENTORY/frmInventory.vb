﻿Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports System.IO
Public Class frmInventory
    Public Sub GetAllProduct()
        Try
            ExecuteCommand("GetAllProduct")
            loadLvw()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub frmInventory_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setLocation(Me)
        frmMain.btnClose.Visible = False
        setCboCat(cboCat)
        GetAllProduct()
        setQty()
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
        frmMain.btnClose.Visible = True
    End Sub

    Private Sub setQty()
        Try
            Dim de As Integer = 0
            Dim sa As Integer = 0
            Dim DT As Integer = 0
            Dim st As Integer = 0
            Dim DC As Integer = 0
            Dim SC As Integer = 0
            Dim total As Integer = 0
           

            lvw1.Items.Clear()
            For i As Short = 0 To lvw.Items.Count - 1
                de = setQty1(lvw.Items(i).SubItems(0).Text, "Getall")
                sa = setQty1(lvw.Items(i).SubItems(0).Text, "Getall1")
                DT = getBeg(lvw.Items(i).SubItems(0).Text, dtpStart.Value, "getBeginningDelivery")
                st = getBeg(lvw.Items(i).SubItems(0).Text.Trim, dtpStart.Value, "getBeginningSales")
                DC = getCurrent(lvw.Items(i).SubItems(0).Text, "getDeliveryByDate")
                SC = getCurrent(lvw.Items(i).SubItems(0).Text, "getSalesByDate")

                lvw1.Items.Add(lvw.Items(i).SubItems(0).Text)
                With lvw1.Items(lvw1.Items.Count - 1)
                    .SubItems.Add(lvw.Items(i).SubItems(1).Text)
                    .SubItems.Add(lvw.Items(i).SubItems(2).Text)
                    .SubItems.Add(lvw.Items(i).SubItems(3).Text)
                    .SubItems.Add(DT - st)
                    .SubItems.Add(DC)
                    .SubItems.Add(SC)
                    total = (((DT - st) + DC) - SC)
                    .SubItems.Add(total)
                End With

            Next
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                  MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function setQty1(ByVal A As String, ByVal B As String) As Integer
        Try


            Dim params() As SqlParameter = _
                      {New SqlParameter("@ID", A)}
            ExecuteCommand(B, True, params)
            If dReader.HasRows Then
                While dReader.Read
                    setQty1 = dReader.Item("D")
                End While
            End If
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
        Return setQty1
    End Function

    Private Sub cboCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCat.SelectedIndexChanged
      
        Dim params() As SqlParameter = _
        {New SqlParameter("@Code", cboCat.Text.Substring(cboCat.Text.IndexOf("-") + 2, cboCat.Text.IndexOf("\") - 4).Trim)}
        ExecuteCommand("SearchByProductCategory", True, params)
        loadLvw()
        
        setQty()
    End Sub
    Private Sub loadLvw()
        lvw.Items.Clear()
        If dReader.HasRows Then
            While dReader.Read
                lvw.Items.Add(dReader.Item("ProductCode").ToString)
                With lvw.Items(lvw.Items.Count - 1)
                    .SubItems.Add(dReader.Item("cat_desc").ToString)
                    .SubItems.Add(dReader.Item("mname").ToString)
                    .SubItems.Add(dReader.Item("model").ToString & "  " & dReader.Item("PartNo").ToString & "  " & dReader.Item("ProductSize").ToString)
                End With
            End While
        End If

        dReader.Close()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If cboCat.Text.Length = 0 Then
            Dim params() As SqlParameter = _
          {New SqlParameter("@Code", txtSearch.Text.Trim)}
            ExecuteCommand("SearchByProductCode", True, params)
            loadLvw()
        Else
            Try
                Dim params() As SqlParameter = _
                {New SqlParameter("@Code", txtSearch.Text.Trim), New SqlParameter("@cat", cboCat.Text.Substring(cboCat.Text.IndexOf("-") + 2, cboCat.Text.IndexOf("\") - 4).Trim)}
                ExecuteCommand("SearchByProductCodeCat", True, params)
                loadLvw()

            Catch ex As Exception
                MsgBox("There has been retrieving supplier record! " + ex.Message, _
                       MsgBoxStyle.Exclamation)
            End Try
           
        End If
        setQty()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If lvw1.SelectedItems.Count = 0 Then
            MsgBox("There is no selected product!", MsgBoxStyle.Information)
            Exit Sub
        End If

        txtId.Text = lvw1.FocusedItem.SubItems(0).Text.Trim
        txtProduct.Text = lvw1.FocusedItem.SubItems(0).Text.Trim & "   " & lvw1.FocusedItem.SubItems(1).Text.Trim _
                   & " " & lvw1.FocusedItem.SubItems(2).Text.Trim & " " & lvw1.FocusedItem.SubItems(3).Text.Trim
        getSalesDATeQty()
        DeliveryDATeQty()
        txtBOxQty()
        txtSoH.Text = lvw1.FocusedItem.SubItems(7).Text.Trim
        txtBq.Text = lvw1.FocusedItem.SubItems(4).Text.Trim

    End Sub
    Private Sub getSalesDATeQty()
        Try
            Dim a As String
            Dim date1 As Date = (dtpStart.Value.ToString.Substring(0, dtpStart.Value.ToString.IndexOf(" ")))
            Dim params() As SqlParameter = _
             {New SqlParameter("@id", lvw1.FocusedItem.SubItems(0).Text.Trim), _
              New SqlParameter("@Date1", date1), _
              New SqlParameter("@Date2", (dtpEnd.Value.ToString.Substring(0, dtpEnd.Value.ToString.IndexOf(" "))))}
            ExecuteCommand("getSalesByDate", True, params)

            lvwSales.Items.Clear()
            If dReader.HasRows Then
                While dReader.Read
                    a = dReader.Item("SalesDate").ToString
                    lvwSales.Items.Add(a.Substring(0, a.IndexOf(" ")))
                    With lvwSales.Items(lvwSales.Items.Count - 1)
                        .SubItems.Add(dReader.Item("qty").ToString)
                    End With
                End While
            End If
            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                     MsgBoxStyle.Exclamation)
        End Try

    End Sub
    Private Sub DeliveryDATeQty()
        Try
            Dim date1 As Date = (dtpStart.Value.ToString.Substring(0, dtpStart.Value.ToString.IndexOf(" ")))


            Dim params() As SqlParameter = _
             {New SqlParameter("@id", lvw1.FocusedItem.SubItems(0).Text.Trim), _
              New SqlParameter("@Date1", date1), _
              New SqlParameter("@Date2", dtpEnd.Value)}
            ExecuteCommand("getDeliveryByDate", True, params)
            Dim a As String
            lvwDelivery.Items.Clear()
            If dReader.HasRows Then
                While dReader.Read
                    a = dReader.Item("DeliveryDate").ToString

                    lvwDelivery.Items.Add(a.Substring(0, a.IndexOf(" ")))
                    With lvwDelivery.Items(lvwDelivery.Items.Count - 1)
                        .SubItems.Add(dReader.Item("qty").ToString)
                    End With
                End While
            End If
            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                     MsgBoxStyle.Exclamation)
        End Try

    End Sub
    Private Sub txtBOxQty()
        Dim qty As Integer = 0
        Dim i As Integer = 0
        For i = 0 To lvwDelivery.Items.Count - 1
            qty = qty + Val(lvwDelivery.Items(i).SubItems(1).Text)
        Next
        txtd.Text = qty

        qty = 0
        For i = 0 To lvwSales.Items.Count - 1
            qty = qty + Val(lvwSales.Items(i).SubItems(1).Text)
        Next
        txtS.Text = qty
    End Sub

    Private Function getBeg(ByVal A As String, ByVal d As Date, ByVal B As String) As Integer
        Try

            Dim date1 As Date = DateAdd(DateInterval.Day, -1, d)
            Dim params() As SqlParameter = _
                      {New SqlParameter("@ID", A), New SqlParameter("@Date", date1)}
            ExecuteCommand(B, True, params)
            If dReader.HasRows Then
                While dReader.Read
                    getBeg = dReader.Item("d")
                End While
            End If
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
        Return getBeg
    End Function

    Private Function getCurrent(ByVal A As String, ByVal B As String) As Integer
        Dim x As Integer = 0
        Dim date1 As Date = DateAdd(DateInterval.Day, -1, dtpStart.Value)
        Try
            Dim params() As SqlParameter = _
             {New SqlParameter("@id", A), _
              New SqlParameter("@Date1", date1), _
              New SqlParameter("@Date2", dtpEnd.Value)}
            ExecuteCommand(B, True, params)

            If dReader.HasRows Then
                While dReader.Read


                    x = x + dReader.Item("qty")

                End While
            End If
            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                     MsgBoxStyle.Exclamation)
        End Try
        Return x
    End Function
    

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        GetAllProduct()
        setQty()
    End Sub

    Private Sub Document_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles Document.PrintPage
        Dim Font_0 As Font = New Font("Verdana", 8, FontStyle.Bold)
        Dim Font_1 As Font = New Font("Verdana", 12, FontStyle.Regular)
        Dim Font_1b As Font = New Font("Verdana", 12, FontStyle.Bold)
        Dim Font_2 As Font = New Font("Verdana", 12, FontStyle.Bold)
        Dim Font_3 As Font = New Font("Verdana", 20, FontStyle.Bold)

        Dim Brush_1 As Brush = Brushes.Black

        Dim Title As String = "INVENTORY REPORT"
        Dim dates As String = "Date :" & dtpStart.Value.ToString.Substring(0, dtpStart.Value.ToString.IndexOf(" ")) _
        & " to " & dtpEnd.Value.ToString.Substring(0, dtpEnd.Value.ToString.IndexOf(" "))
        Dim x As Integer = (800 - (Val(Len(Title)) * 10)) / 2
        Dim a As Integer = (1350 - (Val(Len(dates)) * 10)) / 2
        e.Graphics.DrawString(Title, Font_2, Brush_1, x, 30)
        e.Graphics.DrawString(dates, Font_1, Brushes.CadetBlue, a, 75)


        If lvw1.View = View.Details Then
            PrintDetails(e)
        End If

        'Me.Close()

    End Sub
    Private Sub PrintDetails(ByRef e As System.Drawing.Printing.PrintPageEventArgs)

        Static LastIndex As Integer = 0
        Static CurrentPage As Integer = 0

        Dim DpiGraphics As Graphics = Me.CreateGraphics
        Dim DpiX As Integer = DpiGraphics.DpiX
        Dim DpiY As Integer = DpiGraphics.DpiY

        DpiGraphics.Dispose()

        Dim X, Y As Integer
        Dim TextRect As Rectangle = Rectangle.Empty
        Dim TextLeftPad As Single = CSng(2 * (DpiX / 50))
        Dim ColumnHeaderHeight As Single = CSng(lvw1.Font.Height + (10 * (DpiX / 96)))
        Dim StringFormat As New StringFormat
        Dim PageNumberWidth As Single = e.Graphics.MeasureString(CStr(CurrentPage), lvw1.Font).Width

        StringFormat.FormatFlags = StringFormatFlags.NoWrap
        StringFormat.Trimming = StringTrimming.EllipsisCharacter
        StringFormat.LineAlignment = StringAlignment.Center

        CurrentPage += 1

        X = CInt(50)
        Y = CInt(100)

        For ColumnIndex As Integer = 0 To lvw1.Columns.Count - 1
            TextRect.X = X
            TextRect.Y = Y
            TextRect.Width = lvw1.Columns(ColumnIndex).Width
            TextRect.Height = ColumnHeaderHeight

            e.Graphics.FillRectangle(Brushes.LightGray, TextRect)
            e.Graphics.DrawRectangle(Pens.DarkGray, TextRect)

            TextRect.X += TextLeftPad
            TextRect.Width -= TextLeftPad
            e.Graphics.DrawString(lvw1.Columns(ColumnIndex).Text, lvw1.Font, Brushes.Black, TextRect, StringFormat)
            X += TextRect.Width + TextLeftPad
        Next

        Y += ColumnHeaderHeight + 1
        For i = LastIndex To lvw1.Items.Count - 1

            With lvw1.Items(i)

                X = CInt(50)

                For ColumnIndex As Integer = 0 To lvw1.Columns.Count - 1

                    TextRect.X = X
                    TextRect.Y = Y
                    TextRect.Width = lvw1.Columns(ColumnIndex).Width
                    TextRect.Height = .Bounds.Height

                    If lvw1.GridLines Then
                        e.Graphics.DrawRectangle(Pens.DarkGray, TextRect)
                    End If


                    TextRect.X += TextLeftPad
                    TextRect.Width -= TextLeftPad
                    If ColumnIndex < .SubItems.Count Then
                        e.Graphics.DrawString(.SubItems(ColumnIndex).Text, lvw1.Font, Brushes.Black, TextRect, StringFormat)
                    End If
                    X += TextRect.Width + TextLeftPad
                Next
                Y += .Bounds.Height

            End With
        Next

        e.Graphics.DrawString(CStr(CurrentPage), lvw1.Font, Brushes.Black, (e.PageBounds.Width - PageNumberWidth) / 2, e.PageBounds.Bottom - lvw1.Font.Height * 2)

        StringFormat.Dispose()
        LastIndex = 0
        CurrentPage = 0

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try

            Dim ObjPrinterSetting As New System.Drawing.Printing.PrinterSettings
            PrintPreview.Document = Document
            PrintPreview.PrintPreviewControl.Zoom = 1.0
            PrintPreview.WindowState = FormWindowState.Maximized
            PrintPreview.ShowDialog()



        Catch ex As Exception
            MsgBox(ex.Message.ToString, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub clearALL()
        cboCat.Text = ""
        txtSearch.Text = ""
        txtProduct.Text = ""
        txtBq.Text = "0"
        txtd.Text = "0"
        txtS.Text = "0"
        txtSoH.Text = "0"
        lvwDelivery.Items.Clear()
        lvwSales.Items.Clear()
        lvw1.Items.Clear()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        clearALL()
    End Sub
End Class

