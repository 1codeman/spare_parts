﻿Imports System.Text.RegularExpressions
Public Class frmpayment
    Private Sub frmpayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblC.Text = "0.00"
        txtAT.Text = ""
        txtAT.Focus()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Val(txtAT.Text) < Val(lbltotal.Text) Then
            MsgBox("Insuficient payment", MsgBoxStyle.Information, "INFO")
            Exit Sub
        End If
        lblC.Text = Format(Val(txtAT.Text) - Val(lbltotal.Text), "###,##0.00")
        frmAddSales.lblAT.Text = Format(Val(txtAT.Text), "###,##0.00")
        frmAddSales.lblC.Text = Format(Val(txtAT.Text) - Val(lbltotal.Text), "##0.00")
        frmAddSales.lbltotal1.Text = Format(Val(lbltotal.Text), "###,##0.00")
        frmAddSales.btnSave.Enabled = True

        Me.Dispose()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Dispose()
    End Sub

    Private Sub txtAT_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAT.KeyPress
        If txtAT.Text.Contains(".") = True Then
            If Not Regex.IsMatch(e.KeyChar, "[0-9\b]") Then
                e.Handled = True
            End If
        Else
            If Not Regex.IsMatch(e.KeyChar, "[0-9.\b]") Then
                e.Handled = True
            End If
        End If
    End Sub


End Class