﻿Imports System.Data.SqlClient

Public Class frmSales

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub frmSales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setLocation(Me)
        ExecuteCommand("GetAllSales")
        GetAllsALE()
    End Sub
    Public Sub GetAllsALE()
        Try
            lvwSalesList.Items.Clear()

            Dim a As String

            If dReader.HasRows Then
                While dReader.Read
                    lvwSalesList.Items.Add(dReader.Item("ORNo").ToString)
                    With lvwSalesList.Items(lvwSalesList.Items.Count - 1)
                        a = dReader.Item("SalesDate").ToString
                        .SubItems.Add(a.Substring(0, a.IndexOf(" ")))

                        .SubItems.Add(dReader.Item("cname").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        frmAddSales.lblOR.Text = frmAddSales.GenerateID()
        frmAddSales.ShowDialog(Me)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        frmViewSalesInfo.GetORINFO()
        frmViewSalesInfo.ShowDialog(Me)
    End Sub


    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim params() As SqlParameter = {New SqlParameter("@name", txtSearch.Text.Trim)}
        ExecuteCommand("searchGetAllSalesbyCname", True, params)
        GetAllsALE()
    End Sub

 
    Private Sub dtp1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp1.ValueChanged
        Try
            Dim a As String = dtp1.Value
            a = a.Substring(0, a.IndexOf(" "))
            Dim params() As SqlParameter = {New SqlParameter("@date", a)}
            ExecuteCommand("searchGetAllSalesbyDate", True, params)
            GetAllsALE()
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
        
    End Sub
End Class