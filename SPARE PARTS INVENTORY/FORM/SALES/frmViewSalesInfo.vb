﻿Imports System.Data.SqlClient
Public Class frmViewSalesInfo
    Dim total As Double
    Private Sub frmViewSalesInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setLocation(Me)
    End Sub
    Public Sub GetORINFO()


        Try
            Dim code As Integer = frmSales.lvwSalesList.FocusedItem.SubItems(0).Text
            lblOR.Text = frmSales.lvwSalesList.FocusedItem.SubItems(0).Text
            dtpDate.Value = frmSales.lvwSalesList.FocusedItem.SubItems(1).Text
            Dim params() As SqlParameter = _
                          {New SqlParameter("@OR", code)}


            ExecuteCommand("GetSalesINFO", True, params)

            If dReader.HasRows Then


                While dReader.Read
                    lblId.Text = dReader.Item("ID").ToString
                    lblName.Text = dReader.Item("Cname").ToString
                    lvwSalesList.Items.Add(dReader.Item("ProductCode").ToString)
                    lbladd.Text = dReader.Item("cust_address").ToString
                    With lvwSalesList.Items(lvwSalesList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("cat_desc").ToString & "  " & _
                                      (dReader.Item("mname").ToString) & "  " & _
                                          (dReader.Item("model").ToString & "  " & _
                                           dReader.Item("PartNo").ToString & "  " & _
                                           dReader.Item("ProductSize").ToString))
                        .SubItems.Add(Format(dReader.Item("price"), "##0.00"))
                        .SubItems.Add(dReader.Item("qty").ToString)
                        .SubItems.Add(Format(Val(.SubItems(2).Text) * Val(.SubItems(3).Text), "##0.00"))
                    End With
                    lblAT.Text = dReader.Item("payment").ToString





                End While
            End If

        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
        UPdatetotal()
        lblC.Text = Format(CDbl(lblAT.Text) - CDbl(lblTotal.Text), "###,##0.00")




    End Sub
    Public Sub UPdatetotal()
        total = 0

        For i As Integer = 0 To lvwSalesList.Items.Count - 1
            total = total + CDbl(lvwSalesList.Items(i).SubItems(4).Text)
        Next
        lblTotal.Text = Format(total, "###,##0.00")
        lbltotal1.Text = Format(total, "###,##0.00")
    End Sub

    
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub
    Private Sub Document_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles Document.PrintPage
        Dim Font_0 As Font = New Font("Verdana", 8, FontStyle.Bold)
        Dim Font_1 As Font = New Font("Verdana", 10, FontStyle.Regular)
        Dim Font_1b As Font = New Font("Verdana", 12, FontStyle.Bold)
        Dim Font_2 As Font = New Font("Verdana", 12, FontStyle.Bold)
        Dim Font_3 As Font = New Font("Verdana", 20, FontStyle.Bold)

        Dim Brush_1 As Brush = Brushes.Black

        Dim Title As String = "Iriga Joe Hardware and Auto Supply "
        Dim add As String = "corner Isarog st. Panganiban Drive Naga City"

        Dim x As Integer = (800 - (Val(Len(Title)) * 10)) / 2
        Dim a As Integer = (920 - (Val(Len(add)) * 10)) / 2
        e.Graphics.DrawString(Title, Font_2, Brush_1, x, 30)
        e.Graphics.DrawString(add, Font_1, Brushes.Gray, a, 50)
        e.Graphics.DrawString("No :" & lblOR.Text, Font_1, Brushes.Red, 65, 90)

        e.Graphics.DrawString(dtpDate.Value.ToString.Substring(0, dtpDate.Value.ToString.IndexOf(" ")), lvwSalesList.Font, _
                     Brushes.Black, 600, 90)
        e.Graphics.DrawString("Name : " & lblName.Text, lvwSalesList.Font, _
                                           Brushes.Black, 65, 110)
        e.Graphics.DrawString("Address : " & lbladd.Text, lvwSalesList.Font, _
                                           Brushes.Black, 65, 130)

        If lvwSalesList.View = View.Details Then
            PrintDetails(e)
        End If


        'Me.Close()

    End Sub
    Private Sub PrintDetails(ByRef e As System.Drawing.Printing.PrintPageEventArgs)

        Static LastIndex As Integer = 0
        Static CurrentPage As Integer = 0

        Dim DpiGraphics As Graphics = Me.CreateGraphics
        Dim DpiX As Integer = DpiGraphics.DpiX
        Dim DpiY As Integer = DpiGraphics.DpiY

        DpiGraphics.Dispose()

        Dim X, Y As Integer
        Dim TextRect As Rectangle = Rectangle.Empty
        Dim TextLeftPad As Single = CSng(2 * (DpiX / 50))
        Dim ColumnHeaderHeight As Single = CSng(lvwSalesList.Font.Height + (10 * (DpiX / 96)))
        Dim StringFormat As New StringFormat
        Dim PageNumberWidth As Single = e.Graphics.MeasureString(CStr(CurrentPage), lvwSalesList.Font).Width

        StringFormat.FormatFlags = StringFormatFlags.NoWrap
        StringFormat.Trimming = StringTrimming.EllipsisCharacter
        StringFormat.LineAlignment = StringAlignment.Center

        CurrentPage += 1

        X = CInt(130)
        Y = CInt(170)

        For ColumnIndex As Integer = 1 To lvwSalesList.Columns.Count - 1
            TextRect.X = X
            TextRect.Y = Y
            TextRect.Width = lvwSalesList.Columns(ColumnIndex).Width + 20
            TextRect.Height = ColumnHeaderHeight

            e.Graphics.FillRectangle(Brushes.White, TextRect)
            e.Graphics.DrawRectangle(Pens.White, TextRect)

            TextRect.X += TextLeftPad
            TextRect.Width -= TextLeftPad
            If ColumnIndex = 3 Then
                TextRect.X = TextLeftPad + 60

            End If
            If ColumnIndex = 2 Then
                TextRect.X = 600

            End If
            If ColumnIndex = 4 Then
                TextRect.X = 700

            End If
            If ColumnIndex = 1 Then
                TextRect.Width = lvwSalesList.Columns(ColumnIndex).Width + 20
                e.Graphics.DrawString(" Items" + _
                    vbTab.ToString, lvwSalesList.Font, Brushes.Black, TextRect, StringFormat)
            Else
                e.Graphics.DrawString(lvwSalesList.Columns(ColumnIndex).Text + _
                         vbTab.ToString, lvwSalesList.Font, Brushes.Black, TextRect, StringFormat)
            End If
            X += TextRect.Width + TextLeftPad
        Next

        Y += ColumnHeaderHeight + 1
        For i = LastIndex To lvwSalesList.Items.Count - 1

            With lvwSalesList.Items(i)

                X = CInt(110)

                For ColumnIndex As Integer = 1 To lvwSalesList.Columns.Count - 1

                    TextRect.X = X
                    TextRect.Y = Y
                    TextRect.Width = lvwSalesList.Columns(ColumnIndex).Width + 20
                    TextRect.Height = .Bounds.Height

                    If lvwSalesList.GridLines Then
                        e.Graphics.DrawRectangle(Pens.White, TextRect)
                    End If


                    TextRect.X += TextLeftPad
                    TextRect.Width -= TextLeftPad
                    If ColumnIndex < .SubItems.Count Then
                        If ColumnIndex = 3 Then
                            TextRect.X = TextLeftPad + 60

                        End If
                        If ColumnIndex = 4 Then
                            TextRect.X = 700

                        End If
                        e.Graphics.DrawString(.SubItems(ColumnIndex).Text, lvwSalesList.Font, _
                                              Brushes.Black, TextRect, StringFormat)
                    End If
                    X += TextRect.Width + TextLeftPad
                Next
                Y += .Bounds.Height

            End With
        Next
        TextRect.X = 600
        TextRect.Y = Y + 20
        TextRect.Width = dtpDate.Width + 100
        Dim net As Double = CDbl(lblTotal.Text) / 1.12
        Dim vat As Double = CDbl(lblTotal.Text) - net

        e.Graphics.DrawString("Net : " & Format(net, "###,##0.00").ToString, lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)
        TextRect.Y += 20
        e.Graphics.DrawString("Vat : " & Format(vat, "###,##0.00").ToString, lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)
        TextRect.Y += 20
        e.Graphics.DrawString("Total : " & lbltotal1.Text.ToString, lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)
        TextRect.Y += 20
        e.Graphics.DrawString("Amount tendered : " & lblAT.Text.ToString, lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)
        TextRect.Y += 20
        e.Graphics.DrawString("Change : " & lblC.Text.ToString, lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)
        TextRect.Y += 50
        TextRect.X = 300
        e.Graphics.DrawString("This serve as your sales invoice! ", lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)
        TextRect.Y += 20
        TextRect.X = 360
        e.Graphics.DrawString("Thank you! ", lvwSalesList.Font, _
                                             Brushes.Black, TextRect, StringFormat)

        e.Graphics.DrawString(CStr(CurrentPage), lvwSalesList.Font, Brushes.Black, (e.PageBounds.Width - PageNumberWidth) / 2, e.PageBounds.Bottom - lvwSalesList.Font.Height * 2)

        StringFormat.Dispose()
        LastIndex = 0
        CurrentPage = 0

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ObjPrinterSetting As New System.Drawing.Printing.PrinterSettings
        PrintPreview.Document = Document
        PrintPreview.PrintPreviewControl.Zoom = 1.0
        PrintPreview.WindowState = FormWindowState.Maximized
        PrintPreview.ShowDialog()

    End Sub
End Class