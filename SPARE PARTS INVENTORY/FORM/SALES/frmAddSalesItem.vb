﻿Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Public Class frmAddSalesItem

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        frmAddSales.txtProduct.Text = ""
        frmAddSales.pnl1.Visible = False
        Me.Close()
    End Sub

    Private Sub frmAddItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With frmAddSales.lvwProductList.FocusedItem
            lblProduct.Text = .SubItems(0).Text & " " & .SubItems(1).Text
        End With
        LOADCBO()
    End Sub
    Private Sub LOADCBO()
        With cboQty
            .Items.Clear()
            For i As Integer = 1 To 10
                .Items.Add(i)
            Next
        End With
    End Sub


    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If cboQty.Text.Length = 0 Then
            MsgBox("Invalid Quantity!", MsgBoxStyle.Information, "Info")
            Exit Sub
        End If
        Dim de As Integer = 0
        Dim sa As Integer = 0
        de = setQty1(frmAddSales.lvwProductList.FocusedItem.SubItems(0).Text, "Getall")
        sa = setQty1(frmAddSales.lvwProductList.FocusedItem.SubItems(0).Text, "Getall1")
        de = de - sa
        'MsgBox(de)
        For i As Short = 0 To frmAddSales.lvwSalesList.Items.Count - 1
            If frmAddSales.lvwSalesList.Items(i).SubItems(0).Text = frmAddSales.lvwProductList.FocusedItem.SubItems(0).Text Then
                de = de - Val(frmAddSales.lvwSalesList.Items(i).SubItems(3).Text)
            End If
        Next
        If de < Val(cboQty.Text) Then
            MsgBox("Insufficient Stock! There is only " & de & " available")
            Exit Sub
        End If
        frmAddSales.lvwSalesList.Items.Add(lblProduct.Text.Substring(0, lblProduct.Text.IndexOf(" ")))
        With frmAddSales.lvwSalesList.Items(frmAddSales.lvwSalesList.Items.Count - 1)
            .SubItems.Add(frmAddSales.lvwProductList.FocusedItem.SubItems(1).Text)
            .SubItems.Add(frmAddSales.lvwProductList.FocusedItem.SubItems(2).Text)
            .SubItems.Add(cboQty.Text)
            .SubItems.Add(Format(Val(cboQty.Text) * Val(frmAddSales.lvwProductList.FocusedItem.SubItems(2).Text), "###,##0.00"))
        End With
        If frmAddSales.btnSave.Enabled = True Then
            frmAddSales.btnSave.Enabled = False
        End If

        frmAddSales.txtProduct.Text = ""
        frmAddSales.UPdatetotal()
        frmAddSales.pnl1.Visible = False
        Me.Close()
    End Sub

    Private Sub cboQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboQty.KeyPress
        If Not Regex.IsMatch(e.KeyChar, "[0-9\b]") Then
            e.Handled = True
        End If
    End Sub
    Private Sub setQty()
        Try
            Dim de As Integer = 0
            Dim sa As Integer = 0
            de = setQty1(frmAddSales.lvwProductList.FocusedItem.SubItems(0).Text, "Getall")
            sa = setQty1(frmAddSales.lvwProductList.FocusedItem.SubItems(0).Text, "Getall1")


            If (de - sa) < Val(cboQty.Text) Then
                MsgBox("Insufficient Stock! There is " & de - sa & " available")
                Exit Sub
            End If

        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                  MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function setQty1(ByVal A As String, ByVal B As String) As Integer
        Try


            Dim params() As SqlParameter = _
                      {New SqlParameter("@ID", A)}
            ExecuteCommand(B, True, params)
            If dReader.HasRows Then
                While dReader.Read
                    setQty1 = dReader.Item("D")
                End While
            End If
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
        Return setQty1
    End Function

End Class
