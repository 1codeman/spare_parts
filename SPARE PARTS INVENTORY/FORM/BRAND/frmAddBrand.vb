﻿Imports System.Data.SqlClient
Public Class frmAddBrand
    Private Function GenerateBrandID() As String
        Dim Ccode As String = ""

        ExecuteCommand("GenerateManufacturerID")

        If dReader.HasRows Then
            While dReader.Read
                Ccode = dReader.Item("ID").ToString
            End While
        End If

        Return Ccode
    End Function

    Private Sub ClearFields()
        txtID.Text = ""
        txtname.Text = ""

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES

            Dim code As Integer = CInt(txtID.Text.Trim)
            Dim desc As String = txtname.Text.Trim.ToUpper

            'DATA VALIDATION
            If desc = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If
            If checkBrand() = True Then
                MsgBox("BRAND NAME ALREADY EXIST", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@Mcode", code), _
                                            New SqlParameter("@MName", desc)}


            'EXECUTE SQL COMMAND
            ExecuteCommand("AddManufacturer", params)

            MsgBox("Successfully saved!", _
                   MsgBoxStyle.Information, "SUCCESS")
            If chk.Checked = True Then
                Me.Dispose()
            End If
            'CLEAR TEXT FIELDS
            ClearFields()
            'REFRESH THE BRAND LIST
            frmBrand.GetAllBrand()
            'REGENERATE A NEW BRAND ID
            txtID.Text = GenerateBrandID()

        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in adding a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub frmAddCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ClearFields()
        txtID.Text = GenerateBrandID()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub txtname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtname.KeyPress
        keyCheck(e)
    End Sub
    Private Function checkBrand() As Boolean
        Dim desc As String = txtname.Text.Trim.ToUpper
        Dim params() As SqlParameter = { _
                                        New SqlParameter("@BRAND", desc)}
        ExecuteCommand("CHECKBRAND", True, params)
        If dReader.HasRows Then
            Return True
            Exit Function
        End If
        Return False
    End Function
End Class


