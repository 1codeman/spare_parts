﻿Imports System.Data.SqlClient
Public Class frmBrand

    Public Sub GetAllBrand()
        Try
            lvwBrandList.Items.Clear()

            ExecuteCommand("GetAllManufacturer")

            If dReader.HasRows Then
                While dReader.Read
                    lvwBrandList.Items.Add(dReader.Item("mCode").ToString)
                    With lvwBrandList.Items(lvwBrandList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("mName").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving Brand record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub



    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close() 'Close the form
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        frmAddBrand.ShowDialog(Me)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If lvwBrandList.SelectedItems.Count = 0 Then Exit Sub

        'If lvwBrandList.SelectedItems.Count = 0 Then
        '    MsgBox("There is no currently selected brand", _
        '           MsgBoxStyle.Exclamation, _
        '           "")
        '    Exit Sub


        'SHOW/DISPLAYS THE EDIT FORM
        frmEditBrand.ShowDialog(Me)
        'ENSURE THAT THE ITEM CURRENTLY SELECTED IS VISIBLE
        'AFTER CLOSING EDIT FORM
        lvwBrandList.Select()
        lvwBrandList.FocusedItem.EnsureVisible()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'ENSURES THAT AN ITEM IS SELECTED
            If lvwBrandList.SelectedItems.Count = 0 Then Exit Sub

            'ASK THE USER TO CONFIRM DELETION
            If MsgBox("Are you sure to delete this record?", _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "CONFIRMATION") = MsgBoxResult.Yes Then

                'READY THE DATA FOR DELETION
                Dim ID As Integer = _
                    Val(lvwBrandList.FocusedItem.SubItems(0).Text.Trim)

                'DECLARE AN SQL PARAMETER
                Dim params() As SqlParameter = _
                        {New SqlParameter("@ID", ID)}

                'EXECUTE COMMAND
                ExecuteCommand("DeleteManufacturer", params)

                'REFRESH THE LIST
                GetAllBrand()

                'CONFIRMATION
                MsgBox("Record has been deleted!", _
                       MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("There was an error deleting the record!", _
                   MsgBoxStyle.Critical, "ERROR")
        End Try
    End Sub

    Private Sub frmCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetAllBrand()
    End Sub
End Class

