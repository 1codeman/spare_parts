﻿Imports System.Data.SqlClient
Public Class frmEditBrand
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub PopulateForm()
        With frmBrand.lvwBrandList.FocusedItem
            txtID.Text = .SubItems(0).Text
            txtname.Text = .SubItems(1).Text.ToUpper


        End With
    End Sub

    Private Sub frmEditBrand_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PopulateForm()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim CID As Integer = CInt(txtID.Text.Trim)
            Dim desc As String = txtname.Text.Trim.ToUpper

            'DATA VALIDATION
            If desc = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@MCode", CID), _
                                         New SqlParameter("@Mname", desc)}


            'EXECUTE SQL COMMAND
            ExecuteCommand("UpdateManufacturer", params)

            MsgBox("Brand info Successfully updated !", _
                   MsgBoxStyle.Information, "SUCCESS")
            frmBrand.GetAllBrand()
            Me.Dispose()
        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in modifying a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")

        End Try
    End Sub

    Private Sub txtname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtname.KeyPress
        keyCheck(e)
    End Sub

   
End Class