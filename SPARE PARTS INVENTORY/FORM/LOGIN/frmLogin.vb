﻿Imports System.Data.SqlClient
Public Class frmLogin
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try

            Dim username As String = txtUsername.Text.Trim
            Dim password As String = txtPassword.Text.Trim


            Dim param() As SqlParameter = _
                        {New SqlParameter("@USERNAME", username), _
                        New SqlParameter("@USERPASS", password)}


            Dim sqlComm As New SqlCommand

            OpenConnection()

            With sqlComm
                .Connection = sqlConn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "GetSecurity"
                .Parameters.AddRange(param)
            End With


            dReader = sqlComm.ExecuteReader

            If dReader.HasRows Then
                txtPassword.Text = ""
                txtUsername.Text = ""
                Me.Dispose()
                frmMain.btnLogin.Text = "LOG OUT"
                frmMain.btnLogin.Image = SPARE_PARTS_INVENTORY.My.Resources.Resources.Lock_Close
                frmMain.lockButton(True)
                frmMain.GetAllProduct()
                frmMain.setQty()
            Else
                MsgBox("ACCESS DENIED!", MsgBoxStyle.Exclamation, _
                       "INVALID")
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been an error! " & ex.Message, _
                   MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setLocation(Me)
        txtPassword.Text = ""
        txtUsername.Text = ""
    End Sub


    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click

    End Sub
End Class
