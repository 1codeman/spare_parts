﻿Imports System.Data.SqlClient

Public Class frmAddSupplier
    Private Function GenerateSupplierID() As String
        Dim newSID As String = ""

        ExecuteCommand("GenerateSupplierID")

        If dReader.HasRows Then
            While dReader.Read
                newSID = dReader.Item("newSID").ToString
            End While
        End If

        Return newSID
    End Function

    Private Sub txtContactNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContactNo.KeyPress
        numCheck(e)
    End Sub


    Private Sub ClearFields()
        txtName.Text = ""
        txtContactNo.Text = ""
        txtAddress.Text = ""
    End Sub

  

    Private Sub frmAddStudent_Load(ByVal sender As System.Object, _
    ByVal e As System.EventArgs) Handles MyBase.Load
        ClearFields()
        txtID.Text = GenerateSupplierID()
    End Sub


    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim SID As Integer = CInt(txtID.Text.Trim)
            Dim name As String = txtName.Text.Trim
            Dim address As String = txtAddress.Text.Trim
            Dim contact As String = txtContactNo.Text.Trim

            'DATA VALIDATION
            If name = "" Or _
                address = "" Or contact = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@supplier_id", SID), _
                                            New SqlParameter("@supplier_name", name), _
                                            New SqlParameter("@supplier_address", address), _
                                            New SqlParameter("@contact_no", contact)}

            'EXECUTE SQL COMMAND
            ExecuteCommand("AddSupplier", params)

            MsgBox("Successfully saved!", _
                   MsgBoxStyle.Information, "SUCCESS")

            'CLEAR TEXT FIELDS
            ClearFields()
            'REFRESH THE SUPPLIER LIST
            frmSupplier.GetAllSupplier()
            'REGENERATE A NEW SUPPLIER ID
            txtID.Text = GenerateSupplierID()

        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in adding a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        keyCheck(e)
    End Sub

    
End Class