﻿Imports System.Data.SqlClient
Public Class frmEditSupplier

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Dispose()
    End Sub

    Private Sub PopulateForm()
        With frmSupplier.lvwSupplierList.FocusedItem
            txtID.Text = .SubItems(0).Text
            txtName.Text = .SubItems(1).Text
            txtAddress.Text = .SubItems(2).Text
            txtContactNo.Text = .SubItems(3).Text
           
        End With
    End Sub


    Private Sub txtContactNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContactNo.KeyPress
        numCheck(e)
    End Sub

    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        keyCheck(e)
    End Sub
    Private Sub frmEditSupplier_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateForm()
    End Sub

    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim SID As Integer = CInt(txtID.Text.Trim)
            Dim name As String = txtName.Text.Trim
            Dim address As String = txtAddress.Text.Trim
            Dim contact As String = txtContactNo.Text.Trim

            'DATA VALIDATION
            If name = "" Or _
                address = "" Or contact = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@supplier_id", SID), _
                                            New SqlParameter("@supplier_name", name), _
                                            New SqlParameter("@supplier_address", address), _
                                            New SqlParameter("@contact_no", contact)}

            'EXECUTE SQL COMMAND
            ExecuteCommand("UpdateSupplier", params)

            MsgBox("Supplier info Successfully updated !", _
                   MsgBoxStyle.Information, "SUCCESS")
            Me.Dispose()
            frmSupplier.GetAllSupplier()
        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in modifying a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")

        End Try
    End Sub

    Private Sub txtID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtID.TextChanged

    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged

    End Sub

    Private Sub txtAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAddress.TextChanged

    End Sub

    Private Sub txtContactNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtContactNo.TextChanged

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub lblName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblName.Click

    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click

    End Sub

    Private Sub lblID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblID.Click

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class
