﻿Imports System.Data.SqlClient
Public Class frmSupplier
    Public Sub GetAllSupplier()
        Try
            If rdbID.Checked = True Then
                If txtSearch.Text = "" Then
                    ExecuteCommand("GetAllSupplier")
                Else
                    Dim params() As SqlParameter = _
               {New SqlParameter("@Code", Val(txtSearch.Text.Trim))}

                    ExecuteCommand("SearchBySupID", True, params)

                End If
                
            ElseIf rdbCom.Checked = True Then
                Dim params() As SqlParameter = _
                {New SqlParameter("@Code", txtSearch.Text.Trim)}
                ExecuteCommand("SearchBySupname", True, params)

            Else
                ExecuteCommand("GetAllSupplier")
            End If
            lvwSupplierList.Items.Clear()



            If dReader.HasRows Then
                While dReader.Read
                    lvwSupplierList.Items.Add(dReader.Item("supplier_id").ToString)
                    With lvwSupplierList.Items(lvwSupplierList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("supplier_name").ToString)
                        .SubItems.Add(dReader.Item("supplier_address").ToString)
                        .SubItems.Add(dReader.Item("contact_no").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setLocation(Me)
        GetAllSupplier()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Dispose() 'Close the form
        frmMain.Show()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmAddSupplier.ShowDialog(Me)
    End Sub

 

    Public Sub hideButton(ByVal condition As Boolean)
        btnDelete.Visible = condition
        btnEdit.Visible = condition
    End Sub


    Private Sub btnDelete_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'ENSURES THAT AN ITEM IS SELECTED
            If lvwSupplierList.SelectedItems.Count = 0 Then Exit Sub

            'ASK THE USER TO CONFIRM DELETION
            If MsgBox("Are you sure to delete this record?", _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "CONFIRMATION") = MsgBoxResult.Yes Then

                'READY THE DATA FOR DELETION
                Dim ID As Integer = _
                    Val(lvwSupplierList.FocusedItem.SubItems(0).Text.Trim)

                'DECLARE AN SQL PARAMETER
                Dim params() As SqlParameter = _
                        {New SqlParameter("@ID", ID), New SqlParameter("@type", 2)}

                'EXECUTE COMMAND
                ExecuteCommand("DeleteSupplier", params)

                'REFRESH THE LIST
                GetAllSupplier()

                'CONFIRMATION
                MsgBox("Record has been deleted!", _
                       MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("There was an error deleting the record!", _
                   MsgBoxStyle.Critical, "ERROR")
        End Try
    End Sub

    Private Sub btnSelect_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        'ENSURES THAT AN ITEM IS SELECTED
        If lvwSupplierList.SelectedItems.Count = 0 Then
            MsgBox("You must select supplier")
            Exit Sub
        End If




        frmAddDelivery.lblId.Text = lvwSupplierList.FocusedItem.SubItems(0).Text.Trim
        frmAddDelivery.lblName.Text = lvwSupplierList.FocusedItem.SubItems(1).Text.Trim
        Me.Dispose()
    End Sub

    Private Sub btnEdit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click


        If lvwSupplierList.SelectedItems.Count = 0 Then
            MsgBox("There is no currently selected!", _
                   MsgBoxStyle.Exclamation, _
                   "")
            Exit Sub
        End If

        'SHOW/DISPLAYS THE EDIT FORM
        frmEditSupplier.ShowDialog(Me)
        'ENSURE THAT THE ITEM CURRENTLY SELECTED IS VISIBLE
        'AFTER CLOSING EDIT FORM
        lvwSupplierList.Select()
        lvwSupplierList.FocusedItem.EnsureVisible()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click

        frmAddSupplier.ShowDialog(Me)
    End Sub

    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If btnSelect.Visible = False Then
            btnSelect.Visible = True
        End If
        Me.Dispose()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        GetAllSupplier()
    End Sub
End Class