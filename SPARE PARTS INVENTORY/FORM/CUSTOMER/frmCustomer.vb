﻿Imports System.Data.SqlClient
Public Class frmCustomer

    Public Sub GetAllCustomer()
        Try
            If rdbID.Checked = True Then
                If txtSearch.Text = "" Then
                    ExecuteCommand("GetAllCustomer")
                Else
                    Dim params() As SqlParameter = _
             {New SqlParameter("@Code", Val(txtSearch.Text.Trim))}
                    ExecuteCommand("SearchByCustID", True, params)
                End If
              
            ElseIf rdbName.Checked = True Then
                Dim params() As SqlParameter = _
                {New SqlParameter("@Code", txtSearch.Text.Trim)}
                ExecuteCommand("SearchByCustlname", True, params)

            Else
                ExecuteCommand("GetAllCustomer")
            End If

            lvwCustomerList.Items.Clear()


            If dReader.HasRows Then
                While dReader.Read
                    lvwCustomerList.Items.Add(dReader.Item("cust_ID").ToString)
                    With lvwCustomerList.Items(lvwCustomerList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("Cname").ToString)
                        .SubItems.Add(dReader.Item("cust_address").ToString)
                        .SubItems.Add(dReader.Item("contact").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving customer record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub

    

    Private Sub frmCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setLocation(Me)
        GetAllCustomer()


    End Sub

    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Dispose()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click

        If lvwCustomerList.SelectedItems.Count = 0 Then
            MsgBox("There is no selected customer ", MsgBoxStyle.Information, "INFO")
            Exit Sub
        End If
        frmAddSales.lblId.Text = lvwCustomerList.FocusedItem.SubItems(0).Text.Trim
        frmAddSales.lblName.Text = lvwCustomerList.FocusedItem.SubItems(1).Text.Trim
        frmAddSales.lbladd.Text = lvwCustomerList.FocusedItem.SubItems(2).Text.Trim
        Me.Dispose()

    End Sub

    Private Sub btnNew_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        If rdbID.Checked = True Then
            rdbID.Checked = False
        End If
        If rdbName.Checked = True Then
            rdbName.Checked = False

        End If
        frmAddCustomer.ShowDialog(Me)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If rdbID.Checked = True Then
            rdbID.Checked = False
        End If
        If rdbName.Checked = True Then
            rdbName.Checked = False

        End If
        If lvwCustomerList.SelectedItems.Count = 0 Then
            MsgBox("There is no currently selected customer", _
                   MsgBoxStyle.Exclamation, _
                   "")
            Exit Sub
        End If

        lvwCustomerList.Select()
        frmEditCustomer.ShowDialog(Me)
        lvwCustomerList.FocusedItem.EnsureVisible()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
        If btnSelect.Visible = False Then
            btnSelect.Visible = True
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If rdbID.Checked = True Then
            rdbID.Checked = False
        End If
        If rdbName.Checked = True Then
            rdbName.Checked = False

        End If
        Try
            'ENSURES THAT AN ITEM IS SELECTED
            If lvwCustomerList.SelectedItems.Count = 0 Then Exit Sub

            'ASK THE USER TO CONFIRM DELETION
            If MsgBox("Are you sure to delete this record?", _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "CONFIRMATION") = MsgBoxResult.Yes Then

                'READY THE DATA FOR DELETION
                Dim ID As Integer = _
                    Val(lvwCustomerList.FocusedItem.SubItems(0).Text.Trim)

                'DECLARE AN SQL PARAMETER
                Dim params() As SqlParameter = _
                        {New SqlParameter("@ID", ID), New SqlParameter("@type", 1)}

                'EXECUTE COMMAND
                ExecuteCommand("DeleteCustomer", params)

                'REFRESH THE LIST
                GetAllCustomer()

                'CONFIRMATION
                MsgBox("Record has been deleted!", _
                       MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("There was an error deleting the record!", _
                   MsgBoxStyle.Critical, "ERROR")
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        GetAllCustomer()
    End Sub

  
   
End Class
