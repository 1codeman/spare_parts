﻿Imports System.Data.SqlClient

Public Class frmAddCustomer

    Private Function GenerateCustomerID() As String
        Dim newCID As String = ""

        ExecuteCommand("GenerateCustomerID")

        If dReader.HasRows Then
            While dReader.Read
                newCID = dReader.Item("CnewID").ToString
            End While
        End If

        Return newCID
    End Function

    Private Sub ClearFields()

        txtLname.Text = ""
        txtAddress.Text = ""
        txtContactNo.Text = ""

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim CID As Integer = CInt(txtID.Text.Trim)
            Dim Lname As String = txtLname.Text.Trim.ToUpper

            Dim address As String = txtAddress.Text.Trim.ToUpper
            Dim contact As String = txtContactNo.Text.Trim

            'DATA VALIDATION
            If Lname = "" Or _
                address = "" Or contact = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If


            Dim params() As SqlParameter = {New SqlParameter("@cust_ID", CID), _
                                            New SqlParameter("@lname", Lname), _
                                            New SqlParameter("@cust_address", address), _
                                            New SqlParameter("@contact", contact)}

            'EXECUTE SQL COMMAND
            ExecuteCommand("AddCustomer", params)

            MsgBox("Successfully saved!", _
                   MsgBoxStyle.Information, "SUCCESS")

            'CLEAR TEXT FIELDS
            ClearFields()
            'REFRESH THE CUSTOMER LIST
            frmCustomer.GetAllCustomer()
            'REGENERATE A NEW CUSTOMER ID
            txtID.Text = GenerateCustomerID()

        Catch ex As Exception


           
        End Try
    End Sub

    Private Sub frmAddCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setLocation(Me)
        ClearFields()
        txtID.Text = GenerateCustomerID()
    End Sub

   
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

  


    Private Sub txtLname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLname.KeyPress
        keyCheck(e)
    End Sub

    Private Sub txtContactNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContactNo.KeyPress
        numCheck(e)
    End Sub


End Class
