﻿
Imports System.Data.SqlClient

Public Class frmEditCustomer



    Private Sub PopulateForm()
        With frmCustomer.lvwCustomerList.FocusedItem
            txtID.Text = .SubItems(0).Text


            txtLname.Text = .SubItems(1).Text
            txtAddress.Text = .SubItems(2).Text
            txtContactNo.Text = .SubItems(3).Text

        End With
    End Sub




    Private Sub frmEditCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PopulateForm()
        setLocation(Me)
    End Sub

    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim CID As Integer = CInt(txtID.Text.Trim)
            Dim Lname As String = txtLname.Text.Trim.ToUpper
            Dim address As String = txtAddress.Text.Trim.ToUpper
            Dim contact As String = txtContactNo.Text.Trim

            'DATA VALIDATION
            If Lname = "" Or _
                address = "" Or contact = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@cust_ID", CID), _
                                            New SqlParameter("@lname", Lname), _
                                            New SqlParameter("@cust_address", address), _
                                            New SqlParameter("@contact", contact)}

            'EXECUTE SQL COMMAND
            ExecuteCommand("UpdateCustomer", params)

            MsgBox("Customer info Successfully updated !", _
                   MsgBoxStyle.Information, "SUCCESS")
            Me.Dispose()
            frmCustomer.GetAllCustomer()

        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in modifying a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub
    

    Private Sub txtLname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLname.KeyPress
        keyCheck(e)
    End Sub


    Private Sub txtContactNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContactNo.KeyPress
        numCheck(e)
    End Sub



End Class