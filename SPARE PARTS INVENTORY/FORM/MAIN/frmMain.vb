﻿
Imports System.Data.SqlClient
Public Class frmMain
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Application.Exit()
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        If btnLogin.Text = "LOG IN" Then
            frmLogin.ShowDialog(Me)


        Else
            lockButton(False)

            btnLogin.Text = "LOG IN"
            btnLogin.Image = SPARE_PARTS_INVENTORY.My.Resources.Resources.unlock

        End If


    End Sub


    Private Sub btnCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCustomer.Click
        frmCustomer.btnSelect.Visible = False
        frmCustomer.ShowDialog(Me)

    End Sub

    Public Sub lockButton(ByVal e As Boolean)
        btnCustomer.Enabled = e
        btnDelivery.Enabled = e
        btnProduct.Enabled = e

        btnSupplier.Enabled = e
        btnCategory.Enabled = e
        btnBrand.Enabled = e
        btnSales.Enabled = e
        btnInventory.Enabled = e
        If e = True Then
            state.Text = "ONLINE"
        Else
            state.Text = "OFFLINE"
        End If

    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lockButton(False)

    End Sub

    Private Sub btnBrand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrand.Click
        frmBrand.ShowDialog()
    End Sub

    Private Sub btnInventory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInventory.Click
        frmInventory.ShowDialog(Me)
    End Sub

    Private Sub btnCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCategory.Click
        frmCategory.ShowDialog(Me)
    End Sub

    Private Sub btnSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSupplier.Click
        frmSupplier.btnSelect.Visible = False
        frmSupplier.ShowDialog(Me)
    End Sub

    Private Sub btnDelivery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelivery.Click
        frmDelivery.ShowDialog(Me)
    End Sub

    Private Sub btnSales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSales.Click
        frmSales.ShowDialog(Me)
    End Sub

    Private Sub btnProduct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProduct.Click
        frmProduct.ShowDialog(Me)
    End Sub
    Public Sub GetAllProduct()
        Try
            ExecuteCommand("GetAllProduct")
            loadLvw()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub loadLvw()
        lvw.Items.Clear()
        If dReader.HasRows Then
            While dReader.Read
                lvw.Items.Add(dReader.Item("ProductCode").ToString)
                With lvw.Items(lvw.Items.Count - 1)
                    .SubItems.Add(dReader.Item("cat_desc").ToString)
                    .SubItems.Add(dReader.Item("mname").ToString)
                    .SubItems.Add(dReader.Item("model").ToString & "  " & dReader.Item("PartNo").ToString & "  " & dReader.Item("ProductSize").ToString)
                End With
            End While
        End If

        dReader.Close()
    End Sub



    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Me.Close()
    'End Sub

    'Private Sub frmProductALert_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    GetAllProduct()
    '    setQty()
    'End Sub

    Public Sub setQty()
        Try
            Dim de As Integer = 0
            Dim sa As Integer = 0
            Dim DT As Integer = 0
            Dim st As Integer = 0
            Dim DC As Integer = 0
            Dim SC As Integer = 0
            Dim total As Integer = 0


            lvw1.Items.Clear()
            For i As Short = 0 To lvw.Items.Count - 1
                de = setQty1(lvw.Items(i).SubItems(0).Text, "Getall")
                sa = setQty1(lvw.Items(i).SubItems(0).Text, "Getall1")
                'DT = getBeg(lvw.Items(i).SubItems(0).Text, dtpStart.Value, "getBeginningDelivery")
                'st = getBeg(lvw.Items(i).SubItems(0).Text.Trim, dtpStart.Value, "getBeginningSales")
                'DC = getCurrent(lvw.Items(i).SubItems(0).Text, "getDeliveryByDate")
                'SC = getCurrent(lvw.Items(i).SubItems(0).Text, "getSalesByDate")
                If de - sa <= 1 Then
                    lvw1.Items.Add(lvw.Items(i).SubItems(0).Text)
                    With lvw1.Items(lvw1.Items.Count - 1)
                        .SubItems.Add(lvw.Items(i).SubItems(1).Text)
                        .SubItems.Add(lvw.Items(i).SubItems(2).Text)
                        .SubItems.Add(lvw.Items(i).SubItems(3).Text)
                        '.SubItems.Add(DT - st)
                        '.SubItems.Add(DC)
                        '.SubItems.Add(SC)
                        total = (((DT - st) + DC) - SC)
                        .SubItems.Add(de - sa)
                    End With
                End If


            Next
            If lvw1.Items.Count > 0 Then
                pa.Visible = True
            End If
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                  MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function setQty1(ByVal A As String, ByVal B As String) As Integer
        Try


            Dim params() As SqlParameter = _
                      {New SqlParameter("@ID", A)}
            ExecuteCommand(B, True, params)
            If dReader.HasRows Then
                While dReader.Read
                    setQty1 = dReader.Item("D")
                End While
            End If
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
        Return setQty1
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        pa.Visible = False
    End Sub
End Class
