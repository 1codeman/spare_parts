﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.status = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.state = New System.Windows.Forms.ToolStripStatusLabel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnProduct = New System.Windows.Forms.Button
        Me.btnLogin = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnInventory = New System.Windows.Forms.Button
        Me.btnSales = New System.Windows.Forms.Button
        Me.btnBrand = New System.Windows.Forms.Button
        Me.btnCategory = New System.Windows.Forms.Button
        Me.btnSupplier = New System.Windows.Forms.Button
        Me.btnCustomer = New System.Windows.Forms.Button
        Me.btnDelivery = New System.Windows.Forms.Button
        Me.lbl = New System.Windows.Forms.Label
        Me.lvw = New System.Windows.Forms.ListView
        Me.ColumnHeader16 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader17 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.lvw1 = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader11 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader19 = New System.Windows.Forms.ColumnHeader
        Me.pa = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.status.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pa.SuspendLayout()
        Me.SuspendLayout()
        '
        'status
        '
        Me.status.BackColor = System.Drawing.Color.SeaGreen
        Me.status.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel2, Me.state})
        Me.status.Location = New System.Drawing.Point(0, 426)
        Me.status.Name = "status"
        Me.status.Size = New System.Drawing.Size(1376, 22)
        Me.status.TabIndex = 11
        Me.status.Text = "Status:"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(48, 17)
        Me.ToolStripStatusLabel2.Text = "Status :"
        '
        'state
        '
        Me.state.BackColor = System.Drawing.Color.SeaGreen
        Me.state.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.state.ForeColor = System.Drawing.Color.GreenYellow
        Me.state.Name = "state"
        Me.state.Size = New System.Drawing.Size(53, 17)
        Me.state.Text = "OFFLINE"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnProduct)
        Me.Panel1.Controls.Add(Me.btnLogin)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnInventory)
        Me.Panel1.Controls.Add(Me.btnSales)
        Me.Panel1.Controls.Add(Me.btnBrand)
        Me.Panel1.Controls.Add(Me.btnCategory)
        Me.Panel1.Controls.Add(Me.btnSupplier)
        Me.Panel1.Controls.Add(Me.btnCustomer)
        Me.Panel1.Controls.Add(Me.btnDelivery)
        Me.Panel1.Location = New System.Drawing.Point(0, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1376, 65)
        Me.Panel1.TabIndex = 12
        '
        'btnProduct
        '
        Me.btnProduct.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProduct.ForeColor = System.Drawing.Color.Black
        Me.btnProduct.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.Engrenage
        Me.btnProduct.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnProduct.Location = New System.Drawing.Point(157, 3)
        Me.btnProduct.Name = "btnProduct"
        Me.btnProduct.Size = New System.Drawing.Size(67, 57)
        Me.btnProduct.TabIndex = 4
        Me.btnProduct.Text = "PRODUCT"
        Me.btnProduct.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnProduct.UseVisualStyleBackColor = True
        '
        'btnLogin
        '
        Me.btnLogin.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin.ForeColor = System.Drawing.Color.Black
        Me.btnLogin.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.unlock
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnLogin.Location = New System.Drawing.Point(595, 3)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(67, 57)
        Me.btnLogin.TabIndex = 3
        Me.btnLogin.Text = "LOG IN"
        Me.btnLogin.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.clos
        Me.btnClose.Location = New System.Drawing.Point(1296, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(67, 57)
        Me.btnClose.TabIndex = 2
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnInventory
        '
        Me.btnInventory.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInventory.ForeColor = System.Drawing.Color.Black
        Me.btnInventory.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.INVE
        Me.btnInventory.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnInventory.Location = New System.Drawing.Point(522, 3)
        Me.btnInventory.Name = "btnInventory"
        Me.btnInventory.Size = New System.Drawing.Size(67, 57)
        Me.btnInventory.TabIndex = 1
        Me.btnInventory.Text = "INVENTORY"
        Me.btnInventory.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnInventory.UseVisualStyleBackColor = True
        '
        'btnSales
        '
        Me.btnSales.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSales.ForeColor = System.Drawing.Color.Black
        Me.btnSales.Image = CType(resources.GetObject("btnSales.Image"), System.Drawing.Image)
        Me.btnSales.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSales.Location = New System.Drawing.Point(376, 3)
        Me.btnSales.Name = "btnSales"
        Me.btnSales.Size = New System.Drawing.Size(67, 57)
        Me.btnSales.TabIndex = 1
        Me.btnSales.Text = "SALES"
        Me.btnSales.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSales.UseVisualStyleBackColor = True
        '
        'btnBrand
        '
        Me.btnBrand.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrand.ForeColor = System.Drawing.Color.Black
        Me.btnBrand.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.brand
        Me.btnBrand.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBrand.Location = New System.Drawing.Point(230, 3)
        Me.btnBrand.Name = "btnBrand"
        Me.btnBrand.Size = New System.Drawing.Size(67, 57)
        Me.btnBrand.TabIndex = 1
        Me.btnBrand.Text = "BRAND"
        Me.btnBrand.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBrand.UseVisualStyleBackColor = True
        '
        'btnCategory
        '
        Me.btnCategory.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCategory.ForeColor = System.Drawing.Color.Black
        Me.btnCategory.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.Volume_Manager
        Me.btnCategory.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCategory.Location = New System.Drawing.Point(303, 3)
        Me.btnCategory.Name = "btnCategory"
        Me.btnCategory.Size = New System.Drawing.Size(67, 57)
        Me.btnCategory.TabIndex = 1
        Me.btnCategory.Text = "CATEGORY"
        Me.btnCategory.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCategory.UseVisualStyleBackColor = True
        '
        'btnSupplier
        '
        Me.btnSupplier.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSupplier.ForeColor = System.Drawing.Color.Black
        Me.btnSupplier.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.SUPPLIER
        Me.btnSupplier.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSupplier.Location = New System.Drawing.Point(84, 3)
        Me.btnSupplier.Name = "btnSupplier"
        Me.btnSupplier.Size = New System.Drawing.Size(67, 57)
        Me.btnSupplier.TabIndex = 1
        Me.btnSupplier.Text = "SUPPLIER"
        Me.btnSupplier.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSupplier.UseVisualStyleBackColor = True
        '
        'btnCustomer
        '
        Me.btnCustomer.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCustomer.ForeColor = System.Drawing.Color.Black
        Me.btnCustomer.Image = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.customer1
        Me.btnCustomer.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCustomer.Location = New System.Drawing.Point(11, 3)
        Me.btnCustomer.Name = "btnCustomer"
        Me.btnCustomer.Size = New System.Drawing.Size(67, 57)
        Me.btnCustomer.TabIndex = 1
        Me.btnCustomer.Text = "CUSTOMER"
        Me.btnCustomer.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCustomer.UseVisualStyleBackColor = True
        '
        'btnDelivery
        '
        Me.btnDelivery.Font = New System.Drawing.Font("Verdana", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelivery.ForeColor = System.Drawing.Color.Black
        Me.btnDelivery.Image = CType(resources.GetObject("btnDelivery.Image"), System.Drawing.Image)
        Me.btnDelivery.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDelivery.Location = New System.Drawing.Point(449, 3)
        Me.btnDelivery.Name = "btnDelivery"
        Me.btnDelivery.Size = New System.Drawing.Size(67, 57)
        Me.btnDelivery.TabIndex = 1
        Me.btnDelivery.Text = "DELIVERY"
        Me.btnDelivery.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDelivery.UseVisualStyleBackColor = True
        '
        'lbl
        '
        Me.lbl.BackColor = System.Drawing.Color.SeaGreen
        Me.lbl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.White
        Me.lbl.Location = New System.Drawing.Point(0, 0)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(1388, 25)
        Me.lbl.TabIndex = 47
        Me.lbl.Text = "IRIGA JOE MOTORS SPARE PARTS INVENTORY SYSTEM"
        Me.lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvw
        '
        Me.lvw.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader16, Me.ColumnHeader17, Me.ColumnHeader2, Me.ColumnHeader7})
        Me.lvw.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvw.FullRowSelect = True
        Me.lvw.GridLines = True
        Me.lvw.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvw.Location = New System.Drawing.Point(802, 18)
        Me.lvw.Name = "lvw"
        Me.lvw.Size = New System.Drawing.Size(70, 179)
        Me.lvw.TabIndex = 54
        Me.lvw.UseCompatibleStateImageBehavior = False
        Me.lvw.View = System.Windows.Forms.View.Details
        Me.lvw.Visible = False
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Code"
        Me.ColumnHeader16.Width = 100
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Product Description"
        Me.ColumnHeader17.Width = 300
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Width = 64
        '
        'lvw1
        '
        Me.lvw1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader11, Me.ColumnHeader19})
        Me.lvw1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvw1.FullRowSelect = True
        Me.lvw1.GridLines = True
        Me.lvw1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvw1.Location = New System.Drawing.Point(-1, 28)
        Me.lvw1.Name = "lvw1"
        Me.lvw1.Size = New System.Drawing.Size(787, 220)
        Me.lvw1.TabIndex = 53
        Me.lvw1.UseCompatibleStateImageBehavior = False
        Me.lvw1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Code"
        Me.ColumnHeader1.Width = 70
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Category"
        Me.ColumnHeader3.Width = 128
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "brand"
        Me.ColumnHeader4.Width = 100
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Description"
        Me.ColumnHeader11.Width = 223
        '
        'ColumnHeader19
        '
        Me.ColumnHeader19.Text = "remaining balance                  "
        Me.ColumnHeader19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader19.Width = 250
        '
        'pa
        '
        Me.pa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pa.Controls.Add(Me.Label1)
        Me.pa.Controls.Add(Me.Button1)
        Me.pa.Controls.Add(Me.lvw)
        Me.pa.Controls.Add(Me.lvw1)
        Me.pa.Location = New System.Drawing.Point(295, 159)
        Me.pa.Name = "pa"
        Me.pa.Size = New System.Drawing.Size(789, 287)
        Me.pa.TabIndex = 55
        Me.pa.Visible = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.SeaGreen
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(-1, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(802, 25)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = " out of stock !"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(748, 248)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(38, 34)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.SPARE_PARTS_INVENTORY.My.Resources.Resources.newLogo
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(1376, 448)
        Me.ControlBox = False
        Me.Controls.Add(Me.pa)
        Me.Controls.Add(Me.status)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lbl)
        Me.DoubleBuffered = True
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmMain"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.status.ResumeLayout(False)
        Me.status.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.pa.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents status As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents state As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents btnDelivery As System.Windows.Forms.Button
    Friend WithEvents btnInventory As System.Windows.Forms.Button
    Friend WithEvents btnSales As System.Windows.Forms.Button
    Friend WithEvents btnBrand As System.Windows.Forms.Button
    Friend WithEvents btnCategory As System.Windows.Forms.Button
    Friend WithEvents btnSupplier As System.Windows.Forms.Button
    Friend WithEvents btnCustomer As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents btnProduct As System.Windows.Forms.Button
    Friend WithEvents lvw As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvw1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader19 As System.Windows.Forms.ColumnHeader
    Friend WithEvents pa As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
