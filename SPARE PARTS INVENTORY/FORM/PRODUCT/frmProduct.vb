﻿Imports System.Data.SqlClient
Public Class frmProduct
    Public Sub GetAllProduct()
        Try

            If rbdPC.Checked = True Then
                Dim params() As SqlParameter = _
               {New SqlParameter("@Code", txtSearch.Text.Trim)}

                ExecuteCommand("SearchByProductCode", True, params)

            ElseIf rbdC.Checked = True Then
                Dim params() As SqlParameter = _
                {New SqlParameter("@Code", txtSearch.Text.Trim)}
                ExecuteCommand("SearchByProductCategory", True, params)

            Else
                ExecuteCommand("GetAllProduct")
            End If

           
            lvwProductList.Items.Clear()



            If dReader.HasRows Then
                While dReader.Read
                    lvwProductList.Items.Add(dReader.Item("ProductCode").ToString)
                    With lvwProductList.Items(lvwProductList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("catCode").ToString)
                        .SubItems.Add(dReader.Item("cat_desc").ToString & " \ " & dReader.Item("cat_adv").ToString)
                        .SubItems.Add(dReader.Item("mcode").ToString)
                        .SubItems.Add(dReader.Item("mname").ToString)
                        .SubItems.Add(dReader.Item("model").ToString & " | " & dReader.Item("PartNo").ToString & " | " & dReader.Item("ProductSize").ToString)
                        .SubItems.Add(dReader.Item("price").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub frmProduct_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setLocation(Me)
        GetAllProduct()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        GetAllProduct()
    End Sub


    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        frmAddProduct.ShowDialog(Me)
    End Sub

    Private Sub btnDelete_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            'ENSURES THAT AN ITEM IS SELECTED
            If lvwProductList.SelectedItems.Count = 0 Then
                MsgBox("There is no currently selected product")
                Exit Sub
            End If


            'ASK THE USER TO CONFIRM DELETION
            If MsgBox("Are you sure to delete this record?", _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "CONFIRMATION") = MsgBoxResult.Yes Then

                'READY THE DATA FOR DELETION
                Dim ID As String = lvwProductList.FocusedItem.SubItems(0).Text.Trim

                'DECLARE AN SQL PARAMETER
                Dim params() As SqlParameter = _
                        {New SqlParameter("@ID", ID)}

                'EXECUTE COMMAND
                ExecuteCommand("DeleteProduct", params)

                'REFRESH THE LIST
                GetAllProduct()

                'CONFIRMATION
                MsgBox("Record has been deleted!", _
                       MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("There was an error deleting the record!", _
                   MsgBoxStyle.Critical, "ERROR")
        End Try
    End Sub

    Private Sub btnEdit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvwProductList.SelectedItems.Count = 0 Then
            MsgBox("There is no currently selected product", _
                  MsgBoxStyle.Exclamation, _
                      "")
            Exit Sub
        End If



        'If lvwStudentList.SelectedItems.Count = 0 Then
        '   
        '    Exit Sub


        'SHOW/DISPLAYS THE EDIT FORM
        frmEditProduct.ShowDialog(Me)
        'ENSURE THAT THE ITEM CURRENTLY SELECTED IS VISIBLE
        'AFTER CLOSING EDIT FORM
        lvwProductList.Select()
        lvwProductList.FocusedItem.EnsureVisible()
    End Sub

    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub
End Class