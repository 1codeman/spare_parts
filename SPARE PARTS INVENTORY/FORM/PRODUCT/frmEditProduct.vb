﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Public Class frmEditProduct
    Private Sub PopulateForm()
        With frmProduct.lvwProductList.FocusedItem
            Dim a As String = .SubItems(5).Text
            txtID.Text = .SubItems(0).Text
            txtPrice.Text = Val(.SubItems(6).Text)
            cboCat.SelectedItem = .SubItems(1).Text & " - " & .SubItems(2).Text
            cboMan.SelectedItem = .SubItems(3).Text & " - " & .SubItems(4).Text

            txtModel.Text = a.Substring(0, a.IndexOf("|")).Trim
            a = a.Substring(a.IndexOf("|") + 1).Trim
            txtPartNo.Text = a.Substring(0, a.IndexOf("|"))
            txtsize.Text = a.Substring(a.LastIndexOf("|") + 1)
        End With
    End Sub


    Private Sub frmEditProduct_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setCboCat(cboCat)
        setCboMan(cboMan)

        PopulateForm()

    End Sub

 

    Private Sub txtPrice_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrice.KeyPress
        If txtPrice.Text.Contains(".") = True Then
            If Not Regex.IsMatch(e.KeyChar, "[0-9\b]") Then
                e.Handled = True
            End If
        Else
            If Not Regex.IsMatch(e.KeyChar, "[0-9.\b]") Then
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim a As String
            Dim code As String = txtID.Text.Trim
            Dim catCode As Integer
            Dim man As Integer
            Dim model As String = txtModel.Text.Trim.ToUpper
            Dim size As String = txtsize.Text.Trim
            Dim partNo As String = txtPartNo.Text.ToUpper
            Dim price As Double = CDbl(txtPrice.Text.Trim)
            a = cboCat.Text.Trim.ToString
            a = a.Substring(0, a.IndexOf(" ")).Trim
            catCode = CInt(a)

            a = cboMan.Text.Trim.ToString
            a = a.Substring(0, a.IndexOf(" ")).Trim
            man = CInt(a)

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@ProductCode", code), _
                                             New SqlParameter("@CatCode", catCode), _
                                            New SqlParameter("@Manufacturer", man), _
                                            New SqlParameter("@model", model), _
                                            New SqlParameter("@partNo", partNo), _
                                            New SqlParameter("@ProductSize", size), _
                                            New SqlParameter("@price", price)}


            'EXECUTE SQL COMMAND
            ExecuteCommand("UpdateProduct", params)

            MsgBox("Successfully Updated!", _
                   MsgBoxStyle.Information, "SUCCESS")

            'CLEAR TEXT FIELDS
            'ClearFields()
            'REFRESH THE STUDENT LIST
            Me.Close()
            frmProduct.GetAllProduct()


        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in Updating a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub
End Class