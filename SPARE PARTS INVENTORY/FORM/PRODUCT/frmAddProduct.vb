﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Public Class frmAddProduct


    
    Private Sub frmAddProduct_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setCboCat(cboCat)
        setCboMan(cboMan)

    End Sub

 

   

    Private Sub cboCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCat.SelectedIndexChanged
        Dim a As String = cboCat.SelectedItem.Substring(cboCat.SelectedItem.IndexOf("\") + 1).trim

        txtID.Text = GenerateCOde(a)
    End Sub
    Private Function GenerateCOde(ByVal a As String) As String
        Dim Ccode As String = ""
        Dim var As Integer = 0
        Try

            Dim params() As SqlParameter = _
                          {New SqlParameter("@code", a)}


            ExecuteCommand("GenerateProductCode", True, params)

            If dReader.HasRows Then
                While dReader.Read
                    Ccode = dReader.Item("code").ToString

                    If Ccode = "0" Then
                        Ccode = a & "-" & CStr(var + 1)
                    Else
                        Ccode = Ccode.Substring(Ccode.IndexOf("-") + 1).Trim
                        var = CInt(Ccode) + 1
                        Ccode = a & "-" & CStr(var)
                    End If


                End While
            End If

        Catch ex As Exception
            'MsgBox("There has been retrieving data! " + ex.Message, _
            '      MsgBoxStyle.Exclamation)
        End Try


        Return Ccode

    End Function



    Private Sub txtPrice_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrice.KeyPress
        If txtPrice.Text.Contains(".") = True Then
            If Not Regex.IsMatch(e.KeyChar, "[0-9\b]") Then
                e.Handled = True
            End If
        Else
            If Not Regex.IsMatch(e.KeyChar, "[0-9.\b]") Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim a As String
            Dim code As String = txtID.Text.Trim
            Dim catCode As Integer
            Dim man As Integer
            Dim model As String = txtModel.Text.Trim.ToUpper
            Dim size As String = txtsize.Text.Trim
            Dim partNo As String = txtPartNo.Text.ToUpper
            Dim price As Double = CDbl(txtPrice.Text.Trim)
            a = cboCat.Text.Trim.ToString
            a = a.Substring(0, a.IndexOf(" ")).Trim
            catCode = CInt(a)

            a = cboMan.Text.Trim.ToString
            a = a.Substring(0, a.IndexOf(" ")).Trim
            man = CInt(a)

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@ProductCode", code), _
                                             New SqlParameter("@CatCode", catCode), _
                                            New SqlParameter("@Manufacturer", man), _
                                            New SqlParameter("@model", model), _
                                            New SqlParameter("@partNo", partNo), _
                                            New SqlParameter("@ProductSize", size), _
                                            New SqlParameter("@price", price)}


            'EXECUTE SQL COMMAND
            ExecuteCommand("AddProduct", params)

            MsgBox("Successfully saved!", _
                   MsgBoxStyle.Information, "SUCCESS")

            'CLEAR TEXT FIELDS
            'ClearFields()
            'REFRESH THE STUDENT LIST
            frmProduct.GetAllProduct()


        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in adding a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub
End Class