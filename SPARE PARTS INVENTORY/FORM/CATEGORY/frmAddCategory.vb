﻿Imports System.Data.SqlClient

Public Class frmAddCategory

    Private Function GenerateCategoryID() As String
        Dim Ccode As String = ""

        ExecuteCommand("GenerateCategoryID")

        If dReader.HasRows Then
            While dReader.Read
                Ccode = dReader.Item("ID").ToString
            End While
        End If

        Return Ccode
    End Function

    Private Sub ClearFields()
        txtCode.Text = ""
        txtDesc.Text = ""
        txtAd.Text = ""
        
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES

            Dim code As Integer = CInt(txtCode.Text.Trim)
            Dim desc As String = txtDesc.Text.Trim.ToUpper
            Dim ad As String = txtAd.Text.Trim.ToUpper
            'DATA VALIDATION
            If desc = "" And ad = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If
            If checkCAT() = True Then
                MsgBox(desc & " description already exist!")
                Exit Sub
            End If

            If checkCATa() = True Then
                MsgBox(ad & " initial already exist!")
                Exit Sub
            End If

            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@cat_code", code), _
                                            New SqlParameter("@cat_desc", desc), New SqlParameter("@cat_adv", ad)}


            'EXECUTE SQL COMMAND
            ExecuteCommand("AddCategory", params)

            MsgBox("Successfully saved!", _
                   MsgBoxStyle.Information, "SUCCESS")

            'CLEAR TEXT FIELDS
            ClearFields()
            'REFRESH THE CATEGORY LIST
            frmCategory.GetAllCategory()
            'REGENERATE A NEW CATEGORY ID
            txtCode.Text = GenerateCategoryID()

        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in adding a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub frmAddCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setLocation(Me)
        ClearFields()
        txtCode.Text = GenerateCategoryID()
    End Sub

    Private Sub txtDesc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDesc.KeyPress
        keyCheck(e)
    End Sub

    Private Sub txtAd_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAd.KeyPress
        keyCheck(e)
    End Sub
    Private Function checkCAT() As Boolean
        Dim desc As String = txtDesc.Text.Trim.ToUpper

        Dim params() As SqlParameter = { _
                                        New SqlParameter("@catdesc", desc)}
        ExecuteCommand("CHECKCAT", True, params)
        If dReader.HasRows Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function checkCATa() As Boolean
        Dim AD As String = txtAd.Text.Trim.ToUpper
        Dim params() As SqlParameter = { _
                                        New SqlParameter("@catadv", AD)}
        ExecuteCommand("CHECKCATa", True, params)
        If dReader.HasRows Then
            Return True
        Else
            Return False
        End If
    End Function
 
End Class

