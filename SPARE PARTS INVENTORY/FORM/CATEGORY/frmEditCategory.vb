﻿Imports System.Data.SqlClient
Public Class frmEditCategory
    Private Sub PopulateForm()
        With frmCategory.lvwCategoryList.FocusedItem
            txtCode.Text = .SubItems(0).Text
            txtDesc.Text = .SubItems(1).Text
            txtAd.Text = .SubItems(2).Text


        End With
    End Sub

    Private Sub frmEditCategory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PopulateForm()
    End Sub

    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'INITIALIZE VARIABLES
            Dim CID As Integer = CInt(txtCode.Text.Trim)
            Dim desc As String = txtDesc.Text.Trim
            Dim ad As String = txtAd.Text.Trim

            'DATA VALIDATION
            If desc = "" And ad = "" Then
                MsgBox("Fields cannot be empty!")
                Exit Sub
            End If


            'INSTANTIATE AN SQL PARAMATER
            Dim params() As SqlParameter = {New SqlParameter("@cat_code", CID), _
                                            New SqlParameter("@cat_desc", desc), New SqlParameter("@cat_adv", ad)}


            'EXECUTE SQL COMMAND
            ExecuteCommand("UpdateCategory", params)

            MsgBox("Category info Successfully updated !", _
                   MsgBoxStyle.Information, "SUCCESS")
            frmCategory.GetAllCategory()
            Me.Dispose()
        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in modifying a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")

        End Try
    End Sub
    Private Sub txtDesc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDesc.KeyPress
        keyCheck(e)
    End Sub

    Private Sub txtAd_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAd.KeyPress
        keyCheck(e)
    End Sub
End Class