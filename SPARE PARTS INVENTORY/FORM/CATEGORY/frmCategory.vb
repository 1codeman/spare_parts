﻿Imports System.Data.SqlClient

Public Class frmCategory

    Public Sub GetAllCategory()
        Try
            If rbdCode.Checked = True Then
                If txtSearch.Text = "" Then
                    ExecuteCommand("GetAllCategory")
                Else
                    Dim params() As SqlParameter = _
             {New SqlParameter("@Code", Val(txtSearch.Text.Trim))}

                    ExecuteCommand("SearchBycatCode", True, params)

                End If
              
            ElseIf rbdDesc.Checked = True Then
                Dim params() As SqlParameter = _
                {New SqlParameter("@Code", txtSearch.Text.Trim)}
                ExecuteCommand("SearchBycatDesc", True, params)

            Else
                ExecuteCommand("GetAllCategory")
            End If
            lvwCategoryList.Items.Clear()



            If dReader.HasRows Then
                While dReader.Read
                    lvwCategoryList.Items.Add(dReader.Item("cat_code").ToString)
                    With lvwCategoryList.Items(lvwCategoryList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("cat_desc").ToString)
                        .SubItems.Add(dReader.Item("cat_adv").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
       
    End Sub

    Private Sub frmCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setLocation(Me)
        GetAllCategory()
    End Sub

    Private Sub btnAdd_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        frmAddCategory.ShowDialog(Me)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub

    Private Sub btnEdit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvwCategoryList.SelectedItems.Count = 0 Then
            MsgBox("There is no currently selected!", _
                   MsgBoxStyle.Exclamation, _
                   "")
            Exit Sub
        End If

        frmEditCategory.ShowDialog(Me)
       
        lvwCategoryList.Select()
        lvwCategoryList.FocusedItem.EnsureVisible()
    End Sub

    Private Sub btnDelete_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'ENSURES THAT AN ITEM IS SELECTED
            If lvwCategoryList.SelectedItems.Count = 0 Then
                MsgBox("There is no currently selected!  ", _
                   MsgBoxStyle.Exclamation, _
                   "")
                Exit Sub
            End If


            'ASK THE USER TO CONFIRM DELETION
            If MsgBox("Are you sure to delete this record?", _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "CONFIRMATION") = MsgBoxResult.Yes Then

                'READY THE DATA FOR DELETION
                Dim ID As Integer = _
                    Val(lvwCategoryList.FocusedItem.SubItems(0).Text.Trim)

                'DECLARE AN SQL PARAMETER
                Dim params() As SqlParameter = _
                        {New SqlParameter("@ID", ID)}

                'EXECUTE COMMAND
                ExecuteCommand("DeleteCategory", params)

                'REFRESH THE LIST
                GetAllCategory()

                'CONFIRMATION
                MsgBox("Record has been deleted!", _
                       MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("There was an error deleting the record!", _
                   MsgBoxStyle.Critical, "ERROR")
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        GetAllCategory()
    End Sub
End Class

