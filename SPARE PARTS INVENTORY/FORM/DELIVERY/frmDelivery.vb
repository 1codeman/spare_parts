﻿Imports System.Data.SqlClient
Public Class frmDelivery

    Private Sub frmDelivery_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LOADfORM()

    End Sub
    Private Sub LOADfORM()
        setLocation(Me)
        ExecuteCommand("GetAllDelivery")
        GetAllDelivery()

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvwDeliveryList.Items.Count = 0 Or lvwDeliveryList.SelectedItems.Count = 0 Then
            Exit Sub
        End If
        frmAddDelivery.GetDeliveryINFO()
        frmAddDelivery.disable(False)
        frmAddDelivery.ShowDialog()

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        'frmAddDelivery.txtdr.Text = frmAddDelivery.GenerateID()
        frmAddDelivery.ShowDialog(Me)

    End Sub
    Public Sub GetAllDelivery()
        Try
            lvwDeliveryList.Items.Clear()

            Dim a As String

            If dReader.HasRows Then
                While dReader.Read
                    lvwDeliveryList.Items.Add(dReader.Item("drNo").ToString)
                    With lvwDeliveryList.Items(lvwDeliveryList.Items.Count - 1)
                        a = dReader.Item("deliveryDate").ToString
                        .SubItems.Add(a.Substring(0, a.IndexOf(" ")))
                        .SubItems.Add(dReader.Item("Supplier_name").ToString)
                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub

   

    Private Sub dtp1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp1.ValueChanged
        Try
            Dim a As String = dtp1.Value
            a = a.Substring(0, a.IndexOf(" "))
            Dim params() As SqlParameter = {New SqlParameter("@date", a)}
            ExecuteCommand("SearchGetAllDeliverybyDate", True, params)
            GetAllDelivery()
        Catch ex As Exception
            MsgBox("There has been retrieving record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim params() As SqlParameter = {New SqlParameter("@name", txtSearch.Text.Trim)}
        ExecuteCommand("SearchGetAllDeliverybySup", True, params)
        GetAllDelivery()
    End Sub
End Class