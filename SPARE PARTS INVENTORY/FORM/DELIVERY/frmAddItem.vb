﻿Imports System.Text.RegularExpressions
Public Class frmAddItem


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub frmAddItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With frmAddDelivery.lvwProductList.FocusedItem
            lblProduct.Text = .SubItems(0).Text & " " & .SubItems(1).Text
        End With
        LOADCBO()
    End Sub
    Private Sub LOADCBO()
        With cboQty
            .Items.Clear()
            For i As Integer = 1 To 10
                .Items.Add(i)
            Next
        End With
    End Sub


    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If cboQty.Text.Length = 0 Then
            MsgBox("Invalid Quantity!", MsgBoxStyle.Information, "Info")
            Exit Sub
        End If
        frmAddDelivery.lvwDeliveryList.Items.Add(lblProduct.Text.Substring(0, lblProduct.Text.IndexOf(" ")))
        With frmAddDelivery.lvwDeliveryList.Items(frmAddDelivery.lvwDeliveryList.Items.Count - 1)
            .SubItems.Add(frmAddDelivery.lvwProductList.FocusedItem.SubItems(1).Text)
            .SubItems.Add(cboQty.Text)
        End With

        frmAddDelivery.txtProduct.Text = ""


        Me.Close()
    End Sub

    Private Sub cboQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboQty.KeyPress
        If Not Regex.IsMatch(e.KeyChar, "[0-9\b]") Then
            e.Handled = True
        End If
    End Sub

End Class