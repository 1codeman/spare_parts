﻿Imports System.Data.SqlClient
Public Class frmAddDelivery
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmAddItem.ShowDialog()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        frmSupplier.ShowDialog()
        frmSupplier.hideButton(True)
    End Sub
    Public Sub GetAllProduct()
        Try

            lvwProductList.Items.Clear()

            Dim params() As SqlParameter = _
               {New SqlParameter("@Code", txtProduct.Text.Trim)}

            ExecuteCommand("SearchByProductCode", True, params)

            If dReader.HasRows Then
                While dReader.Read
                    lvwProductList.Items.Add(dReader.Item("ProductCode").ToString)
                    With lvwProductList.Items(lvwProductList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("cat_desc").ToString & "  " & (dReader.Item("mname").ToString) & "  " & (dReader.Item("model").ToString & "  " & dReader.Item("PartNo").ToString & "  " & dReader.Item("ProductSize").ToString))


                    End With
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub frmAddDelivery_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        setLocation(Me)
        pnl1.Visible = False

    End Sub


    Private Sub txtProduct_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProduct.TextChanged
        pnl1.Visible = True
        GetAllProduct()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If lvwProductList.SelectedItems.Count = 0 Then
            Exit Sub
        End If
        frmAddItem.ShowDialog()
        pnl1.Visible = False

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnl1.Visible = False
    End Sub


    Private Sub lvwProductList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwProductList.DoubleClick
        btnSelect.PerformClick()
    End Sub


    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lvwDeliveryList.Items.Count = 0 Then Exit Sub
        If lvwDeliveryList.SelectedItems.Count = 0 Then Exit Sub


        Dim selected As Integer = lvwDeliveryList.SelectedItems(0).Index
        lvwDeliveryList.Items(selected).Remove()

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If btnSearch.Enabled = False Then
            disable(True)
        End If
        Me.Dispose()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If lblName.Text.Length < 1 Then
                MsgBox("Invalid! Select supplier")
                Exit Sub

            End If
            If lvwDeliveryList.Items.Count = 0 Then
                MsgBox("Empty delivery Item")
                Exit Sub
            End If
            With lvwDeliveryList
                For i As Short = 0 To .Items.Count - 1
                    Dim DRNO As Integer = Val(txtdr.Text)
                    Dim supplier As Integer = Val(lblId.Text)
                    Dim code As String = .Items(i).SubItems(0).Text
                    Dim qty As Integer = Val(.Items(i).SubItems(2).Text)
                    Dim d As Date = (dtpDate.Value.ToString.Substring(0, dtpDate.Value.ToString.IndexOf(" ")))

                    Dim params() As SqlParameter = {New SqlParameter("@drNo", DRNO), _
                                           New SqlParameter("@productCode", code), New SqlParameter("@ID", supplier), New SqlParameter("@date", d), New SqlParameter("@qty", qty)}


                    'EXECUTE SQL COMMAND
                    ExecuteCommand("AddDelivery", params)
                Next
            End With

            MsgBox("Successfully saved!", _
                   MsgBoxStyle.Information, "SUCCESS")
            disable(True)
            Me.Dispose()
            ExecuteCommand("GetAllDelivery")
            frmDelivery.GetAllDelivery()


        Catch ex As Exception
            Dim msg As String

            msg = "ERROR#" & Err.Number.ToString & _
            " There was an error in adding a record. " & _
               ex.Message

            MsgBox(msg, MsgBoxStyle.Exclamation, "ERROR")
        End Try

    End Sub
    'Public Function GenerateID() As String
    '    Dim Ccode As String = ""

    '    ExecuteCommand("GenerateDRno")

    '    If dReader.HasRows Then
    '        While dReader.Read
    '            Ccode = dReader.Item("ID").ToString
    '        End While
    '    End If

    '    Return Ccode
    'End Function


    Public Sub GetDeliveryINFO()

      
        Try
            Dim code As Integer = frmDelivery.lvwDeliveryList.FocusedItem.SubItems(0).Text
            txtdr.Text = frmDelivery.lvwDeliveryList.FocusedItem.SubItems(0).Text
            dtpDate.Value = frmDelivery.lvwDeliveryList.FocusedItem.SubItems(1).Text
            Dim params() As SqlParameter = _
                          {New SqlParameter("@DR", code)}


            ExecuteCommand("GetDeliveryINFO", True, params)

            If dReader.HasRows Then


                While dReader.Read
                    lblId.Text = dReader.Item("ID").ToString
                    lblName.Text = dReader.Item("Supplier_name").ToString
                    lvwDeliveryList.Items.Add(dReader.Item("ProductCode").ToString)
                
                    With lvwDeliveryList.Items(lvwDeliveryList.Items.Count - 1)
                        .SubItems.Add(dReader.Item("cat_desc").ToString & "  " & _
                                      (dReader.Item("mname").ToString) & "  " & _
                                          (dReader.Item("model").ToString & "  " & _
                                           dReader.Item("PartNo").ToString & "  " & _
                                           dReader.Item("ProductSize").ToString))
                        .SubItems.Add(dReader.Item("qty").ToString)
                    End With




                End While
            End If

        Catch ex As Exception
           
        End Try





    End Sub


    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lvwDeliveryList.Items.Clear()
        lblId.Text = ""
        lblName.Text = ""
        txtdr.Text = ""

    End Sub
    Public Sub disable(ByVal a As Boolean)
        btnSearch.Enabled = a
        btnClear.Enabled = a
        btnOk.Enabled = a
        btnRemove.Enabled = a
        dtpDate.Enabled = a
        Panel3.Enabled = a
        txtdr.Enabled = a
    End Sub


End Class