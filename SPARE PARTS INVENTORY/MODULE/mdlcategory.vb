﻿Module mdlcategory
    Public Sub setCboCat(ByVal cbocat As ComboBox)
        cbocat.Items.Clear()

        Try
            cbocat.Items.Clear()

            ExecuteCommand("GetAllCategory")

            If dReader.HasRows Then
                While dReader.Read
                    cbocat.Items.Add(dReader.Item("cat_code") & " - " & dReader.Item("cat_desc").ToString & " \ " & dReader.Item("cat_adv").ToString)
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving supplier record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Public Sub setCboMan(ByRef cboman As ComboBox)
        Try
            cboman.Items.Clear()

            ExecuteCommand("GetAllManufacturer")

            If dReader.HasRows Then
                While dReader.Read
                    cboman.Items.Add(dReader.Item("mCode").ToString & " - " & dReader.Item("mName").ToString)
                End While
            End If

            dReader.Close()
        Catch ex As Exception
            MsgBox("There has been retrieving Manufacturer record! " + ex.Message, _
                   MsgBoxStyle.Exclamation)
        End Try

    End Sub
End Module
