﻿Imports System.Text.RegularExpressions
Module mdlSetTopLocation
    Public Sub setLocation(ByVal FORM)
        FORM.top = frmMain.Panel1.Height + 30
    End Sub
    Public Sub keyCheck(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not Regex.IsMatch(e.KeyChar, "[a-zA-Z \-\b]") Then
            e.Handled = True
        End If
    End Sub
    Public Sub numCheck(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not Regex.IsMatch(e.KeyChar, "[0-9 \-\b]") Then
            e.Handled = True
        End If
    End Sub
    Public Sub adDCheck(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not Regex.IsMatch(e.KeyChar, "[0-9a-zA-Z#,. \-\b]") Then
            e.Handled = True
        End If
    End Sub
    Public Sub numOnly(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not Regex.IsMatch(e.KeyChar, "[0-9 \-\b]") Then
            e.Handled = True
        End If
    End Sub
End Module
